import * as React from "react";
import { Chart } from "react-google-charts";
 
const ExampleChart = () => {
  return (
    <Chart
  width={'1400px'}
  height={'400px'}
  chartType="LineChart"
  loader={<div>Loading Chart</div>}
  data={[
    ['x', 'Assessment Scope' , 'Cumu. Rollout Plan', 'Assessments Actuals'],
    ["Jun", 11347, 7681, 4467],
    ["Jul", 11347, 9438, 'null'],
    ["Aug", 11347, 11066, 'null'],
    ["Sept", 11347, 11347, 'null'],
  ]}
  options={{
    hAxis: {
      title: '',
    },
    vAxis: {
      title: '',
    },
    series: {
      1: { curveType: 'function' },
    },
  }}
  rootProps={{ 'data-testid': '2' }}
/>  
  );
};
export default ExampleChart;